
/*
 *  winston-opbeat.js: Winston Transport for Opbeat (opbeat.com)
 *
 *  Pramp Inc. is not affiliated with Opbeat or ElasticSearch.
 *  (C) 2017 Pramp Inc.
 *  Apache-2 License
 *
 */

import Winston from 'winston';
import {isError} from 'core-util-is';

export default class WinstonOpbeat extends Winston.Transport{
    constructor(options) {
        super(options);
        const {opbeat, level, name} = options;
        this.opbeat = opbeat;
        this.name = name || 'opbeat';
        this.level = level || 'error';
    }

    /**
     *  Pass errors in the meta.error fields.
     *  Pass additional data in meta.info.
     */
    log = (level, msg, meta, callback) => {
        let extra = {};
        let error;

        // handle when meta field isn't included
        if (typeof(meta) === 'function' && !callback) {
            callback = meta;
            meta = false;
        }

        if (!this.opbeat) {
            return callback(null, true);
        }

        if (meta && meta.info) {
            extra.info = meta.info;
        }

        if (meta && meta.error && isError(meta.error)) {
            error = meta.error;
            extra.message = msg;
        } else {
            error = msg;
        }

        // noinspection JSUnresolvedFunction
        this.opbeat.captureError(error, {
            extra: extra
        });

        // noinspection JSUnresolvedFunction
        this.emit('logged');
        callback(null, true);
    };
}