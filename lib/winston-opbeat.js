'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _winston = require('winston');

var _winston2 = _interopRequireDefault(_winston);

var _coreUtilIs = require('core-util-is');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
/*
 *  winston-opbeat.js: Winston Transport for Opbeat (opbeat.com)
 *
 *  Pramp Inc. is not affiliated with Opbeat or ElasticSearch.
 *  (C) 2017 Pramp Inc.
 *  Apache-2 License
 *
 */

var WinstonOpbeat = function (_Winston$Transport) {
    _inherits(WinstonOpbeat, _Winston$Transport);

    function WinstonOpbeat(options) {
        _classCallCheck(this, WinstonOpbeat);

        var _this = _possibleConstructorReturn(this, (WinstonOpbeat.__proto__ || Object.getPrototypeOf(WinstonOpbeat)).call(this, options));

        _this.log = function (level, msg, meta, callback) {
            var extra = {};
            var error = void 0;

            // handle when meta field isn't included
            if (typeof meta === 'function' && !callback) {
                callback = meta;
                meta = false;
            }

            if (!_this.opbeat) {
                return callback(null, true);
            }

            if (meta && meta.info) {
                extra.info = meta.info;
            }

            if (meta && meta.error && (0, _coreUtilIs.isError)(meta.error)) {
                error = meta.error;
                extra.message = msg;
            } else {
                error = msg;
            }

            // noinspection JSUnresolvedFunction
            _this.opbeat.captureError(error, {
                extra: extra
            });

            // noinspection JSUnresolvedFunction
            _this.emit('logged');
            callback(null, true);
        };

        var opbeat = options.opbeat,
            level = options.level,
            name = options.name;

        _this.opbeat = opbeat;
        _this.name = name || 'opbeat';
        _this.level = level || 'error';
        return _this;
    }

    /**
     *  Pass errors in the meta.error fields.
     *  Pass additional data in meta.info.
     */


    return WinstonOpbeat;
}(_winston2.default.Transport);

exports.default = WinstonOpbeat;